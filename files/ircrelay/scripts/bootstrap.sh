#!/bin/bash

###################
apt-get update
apt-get -y upgrade
apt-get -y install golang git procps
###################
mkdir /srv/ircrelay/
cd /srv/
git clone https://github.com/google/alertmanager-irc-relay
mv alertmanager-irc-relay ircrelay-src
cd ircrelay-src
go build
mv alertmanager-irc-relay /srv/ircrelay/alertmanager
###################
cp -fv /var/tmp/config.yml /srv/ircrelay/config.yml
chmod -v 644 /srv/ircrelay/*
chmod -v 755 /srv/ircrelay/alertmanager

