#!/bin/bash

##############################
# Start Daemon
##############################

while true; do

  printf "Checking daemon.."
  if [[ `ps aux | grep 'alertmanager' \
        | wc -l` -le 1 ]]; then

    echo "[Down]"
    printf "Starting daemon..."
    /srv/ircrelay/alertmanager --config /srv/ircrelay/config.yml
  fi

  echo "[Ok]"
  sleep 10
done

